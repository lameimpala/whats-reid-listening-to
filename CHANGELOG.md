## [0.29.2](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.29.1...v0.29.2) (2025-01-17)


### Bug Fixes

* malformed .js config file ([859df4e](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/859df4e6e9ea4c80e6f6d8980e575d5cc1304e1b))

## [0.29.1](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.29.0...v0.29.1) (2025-01-17)


### Bug Fixes

* change secret name and value ([fe9e242](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/fe9e2424c91d69cc4087ea8d990e6f9a70221e93))

# [0.29.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.28.6...v0.29.0) (2024-12-14)


### Features

* update styles and auto-deploy ([2ce3416](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/2ce3416d3cb4f37577c85fb98eba6e34724c080e))

## [0.28.6](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.28.5...v0.28.6) (2024-12-14)


### Bug Fixes

* gitlab ci docker ([2e0ea59](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/2e0ea595994d17b5751bf6bb87755f878db11dac))

## [0.28.5](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.28.4...v0.28.5) (2024-12-14)


### Bug Fixes

* gitlab ci docker ([e12cfc9](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/e12cfc9632e4a66f7b379b79809d4761d2912ca0))

## [0.28.4](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.28.3...v0.28.4) (2024-12-14)


### Bug Fixes

* gitlab ci docker ([ce1fa8c](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/ce1fa8c1a89411d44fef29e3cad4ff378174f5dd))

## [0.28.3](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.28.2...v0.28.3) (2024-12-14)


### Bug Fixes

* gitlab ci docker ([459919b](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/459919bbc7bd5b9de691dfbd6934501a3b91fe1e))

## [0.28.2](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.28.1...v0.28.2) (2024-12-14)


### Bug Fixes

* nudge gitlab-ci.yml ([030261b](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/030261b4272160e1387d66789997e4954aa890e1))

## [0.28.1](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.28.0...v0.28.1) (2024-12-14)


### Bug Fixes

* nudge gitlab-ci.yml ([c341504](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/c3415042df857f4590909337c4142a724ac6ff39))

# [0.28.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.27.0...v0.28.0) (2024-12-14)


### Features

* add support for apple music and update styles and fonts ([b5127a2](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/b5127a22b40a0ec662d8e54069e345dbe69f5d6c))

# [0.27.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.26.0...v0.27.0) (2024-06-12)


### Features

* ui tweaks ([fd72645](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/fd72645784345f8cececf2c52e573fa9f1ce1ed9))

# [0.26.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.25.0...v0.26.0) (2024-06-12)


### Features

* add theme color and adjust columns ([30cf53f](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/30cf53f4c124eaab0bd6fab2440f5accaa19642f))
* bump version to 1.0.0 as design is complete ([513529b](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/513529b5c128d79293fd3737f833e9bf96996993))

# [0.25.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.24.0...v0.25.0) (2024-06-12)


### Features

* aesthetic enhancements ([64b93ce](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/64b93ce6bca72adbdce3f748dfa1a8203e7417e4))

# [0.24.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.23.0...v0.24.0) (2024-06-11)


### Features

* style overhaul ([d8ec387](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/d8ec38798103993ad22a0c121a3c26341b1131a2))

# [0.23.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.22.0...v0.23.0) (2024-06-11)


### Features

* add new fonts: ([b489bf5](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/b489bf50265af954e687fd3a0d9bc5bf263ba577))

# [0.22.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.21.1...v0.22.0) (2024-05-08)


### Features

* remove react router and add links ([1f2ba8d](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/1f2ba8d4e852feafc77afb6b6e1950780e9a7c98))

## [0.21.1](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.21.0...v0.21.1) (2024-05-07)


### Bug Fixes

* padding ([7a7b640](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/7a7b6401414f3d6f066b4dbe6984552c721e558a))

# [0.21.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.20.1...v0.21.0) (2024-05-07)


### Bug Fixes

* bump versions ([b8145c4](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/b8145c4910b341261868f78fef760f00a0622321))


### Features

* split generated CSS into its own file to boost initial page load performance ([9056da1](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/9056da15b36e3f1f0eba32ed63500d36cf1d7210))
* split generated CSS into its own file to boost initial page load performance ([aa86af3](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/aa86af3774b393f3712ce1c326c0c3f14bf77e05))

## [0.20.1](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.20.0...v0.20.1) (2024-05-07)


### Bug Fixes

* remove extraneous sass property ([a808a9f](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/a808a9fc03dc226a15cd1b54a5bd16d5d7d2334c))

# [0.20.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.19.0...v0.20.0) (2024-05-07)


### Features

* add better tech info modal ([4331b46](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/4331b46693174661ac79ceee085c02acd878f51a))

# [0.19.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.18.0...v0.19.0) (2023-04-02)


### Features

* increase top artist count ([6a799c7](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/6a799c7210ca23e3333a5756fb74b2c63007145e))

# [0.18.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.17.0...v0.18.0) (2023-03-28)


### Bug Fixes

* use node instead of npm start in docker cmd ([bc3c1a7](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/bc3c1a7e0fab2e5d3067901aa2ab4bbf3c7044aa))


### Features

* improve responsiveness with columns for songs and artists ([4da6f2c](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/4da6f2c75f833e5f83efbfbd78257fa030765aa6))

# [0.17.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.16.0...v0.17.0) (2023-03-25)


### Bug Fixes

* linting ([4a22d6a](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/4a22d6aa19883912684268ed8f7f07007f35fc85))


### Features

* add top artists and refactor ([d4658c2](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/d4658c20e4d16b990bb8f63c633857c41429af2b))

# [0.16.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.15.0...v0.16.0) (2022-09-19)


### Features

* change style of plays ([aefcc3c](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/aefcc3c42fe58e15b5e681261ac2bb74470a8a49))
* change style of plays ([327742c](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/327742c7e461756dcac9de701a9b0f23f29045dc))

# [0.15.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.14.1...v0.15.0) (2022-09-19)


### Features

* change play count to 60 ([8af34eb](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/8af34eb4df833a2ce8c7d474dda1e85fc3908206))

## [0.14.1](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.14.0...v0.14.1) (2022-09-19)


### Bug Fixes

* fix tsc errors ([7804ae7](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/7804ae7ed175e829ad2eb7535763b3c3f9b9f237))

# [0.14.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.13.0...v0.14.0) (2022-09-19)


### Features

* add modal ([cf88d26](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/cf88d26c14ab2183272f587d51322bb843f09460))

# [0.13.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.12.0...v0.13.0) (2022-09-19)


### Features

* remove blog and update readme ([379269d](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/379269dd2796e34aef9fa0e25fbbe6f872721089))

# [0.12.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.11.0...v0.12.0) (2022-08-06)


### Features

* change Plays to 4-wide columns ([60da6c7](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/60da6c747018c3fe50e6a0b3a959f37e8e57288f))

# [0.11.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.10.0...v0.11.0) (2022-07-31)


### Features

* include play counts ([08cd53c](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/08cd53c28497c978d0914ccfe87e147cbee71b12))

# [0.10.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.9.0...v0.10.0) (2022-07-31)


### Features

* split cards into 2 columns ([536342a](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/536342ac6ad88b358cddef514241880f0bdc7fe6))

# [0.9.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.8.0...v0.9.0) (2022-07-31)


### Features

* add links ([65b3736](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/65b3736f7565496f74f5c7dccab51210db15b406))

# [0.8.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.7.0...v0.8.0) (2022-07-31)


### Features

* add favicon ([0797dd0](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/0797dd0746bf3109b2ebb029c4fa9d4d835e3024))

# [0.7.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.6.1...v0.7.0) (2022-07-31)


### Features

* add fontawesome ([737493f](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/737493f966105f43639df6f2f4cbc41078d48e12))

## [0.6.1](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.6.0...v0.6.1) (2022-07-31)


### Bug Fixes

* left align cards ([572c1d1](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/572c1d1050def7e6e2269cc31db55a60a097253f))

# [0.6.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.5.0...v0.6.0) (2022-07-31)


### Features

* improve styling of landing page ([5475161](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/5475161b03b2568cbae7bbe4286b1d38f80edec4))

# [0.5.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.4.0...v0.5.0) (2022-07-30)


### Features

* fetch plays from api ([147aafc](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/147aafc259ddf97c1b8db4b61f770da20a8b0f6e))

# [0.4.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.3.2...v0.4.0) (2022-07-30)


### Features

* add post ([6e3ba20](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/6e3ba2087bcdee4cd1d3d8d4cae62fa4fd8667e3))

## [0.3.2](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.3.1...v0.3.2) (2022-07-30)


### Bug Fixes

* build as prod ([da5cb27](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/da5cb27bb00e1f2a14b8746a00e6c5bb2cb7ff65))

## [0.3.1](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.3.0...v0.3.1) (2022-07-30)


### Bug Fixes

* remove ci skipper from auto tagging ([ee1c7e5](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/ee1c7e53ca72928f4b44e5c6ff9ca1e591495e5e))

# [0.3.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.2.0...v0.3.0) (2022-07-30)


### Bug Fixes

* add changelog to tagging ([5844401](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/5844401a62a9c213500053a99b099757016bbc72))
* change order of plugins ([0317dc5](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/0317dc5295e20d4808e05b6089a12a98b2fdea71))
* disable husky in CI ([2a28160](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/2a2816057eab64129bb55f2dae53364d38bb9a44))
* install dependencies ([109a2f6](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/109a2f68a7d12a7abcce9eb65ecb61beb3cb72bc))
* use tag instead of latest when deploying image ([7e8b415](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/7e8b4156348b74308d6c6ec6535b9e351151ab52))


### Features

* bump package.json when creating tags ([b64adf9](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/b64adf902d78e9fc136e7c58441a6b548ca9bb67))
