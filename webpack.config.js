const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const config = require("config");

const devtool = config.get("shouldUseDevelopmentFeatures")
  ? { devtool: "source-map" }
  : null;

module.exports = {
  ...devtool,
  mode: process.env.NODE_ENV || "development",
  entry: ["./src/front/index"],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.s[ac]ss$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: "asset/resource",
      },
    ],
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist", "front"),
    publicPath: "/",
  },
  plugins: [
    new MiniCssExtractPlugin(),
    new HtmlWebpackPlugin({
      template: "./src/front/index.ejs",
      favicon: "./src/front/favicon.ico",
      templateParameters: {
        shouldUseAnalytics: !config.get("shouldUseDevelopmentFeatures"),
      },
    }),
  ],
};
