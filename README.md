# What's Reid Listening To?

See it @ [whatsreidlistening.to](https://whatsreidlistening.to)

## Technologies

* node.js with express and mongoose for backend, hosted on Google Cloud Run
* docker for containerization
* mongodb for data store, hosted on Mongo Atlas
* react for frontend, bundled with webpack
