import config from "config";
import mongoose from "mongoose";

import app from "./app";

require("dotenv").config();

(async () => {
  await mongoose.connect(`${process.env.MONGO_CONNECTION_STRING}`);

  app.listen(config.get("httpPort"), () => {
    console.log("Listening on... ", config.get("httpPort"));
  });
})();
