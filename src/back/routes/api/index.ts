import { Router } from "express";
import Play from "./play";

const PAGE_SIZE = 8;

const router = Router();

router.get("/last-played-at", async (req, res) => {
  const lastPlayedSong = await Play.findOne().sort({ time: -1 });
  res.json(lastPlayedSong.time);
});

router.get("/plays/:page", async (req, res) => {
  const plays = await Play.aggregate()
    .group({
      _id: "$song",
      song: { $last: "$song" },
      artist: { $last: "$artist" },
      album: { $last: "$album" },
      time: { $last: "$time" },
      imageUrl: { $last: "$imageUrl" },
      imageData: { $last: "$imageData" },
      lyrics: { $last: "$lyrics" },
      wikipedia: { $last: "$wikipedia" },
      artistWikipedia: { $last: "$artistWikipedia" },
      plays: { $count: {} },
    })
    .sort({ time: -1 })
    .skip(parseInt(req.params.page) * PAGE_SIZE)
    .limit(PAGE_SIZE);

  res.json(plays);
});

router.get("/artists", async (req, res) => {
  const artists = await Play.aggregate()
    .group({
      _id: "$artist",
      artist: { $last: "$artist" },
      time: { $last: "$time" },
      imageUrl: { $last: "$imageUrl" },
      album: { $last: "$album" },
      plays: { $count: {} },
    })
    .sort({ plays: -1 })
    .limit(17);
  res.json(
    artists
      .filter(({ artist }: { artist: string }) => artist !== "artist")
      .slice(0, 16)
  );
});

export default router;
