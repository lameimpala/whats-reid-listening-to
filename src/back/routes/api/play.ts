import mongoose from "mongoose";

const schema = new mongoose.Schema({
  song: "string",
  artist: "string",
  album: "string",
  time: "number",
  lyrics: "string",
  wikipedia: "string",
  artistWikipedia: "string",
});

export default mongoose.model("Play", schema);
