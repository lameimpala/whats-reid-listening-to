import { useState } from "react";
import classnames from "classnames";
import { faDocker } from "@fortawesome/free-brands-svg-icons/faDocker";
import { faReact } from "@fortawesome/free-brands-svg-icons/faReact";
import { faGoogle } from "@fortawesome/free-brands-svg-icons/faGoogle";
import { faCss3 } from "@fortawesome/free-brands-svg-icons/faCss3";
import { faNodeJs } from "@fortawesome/free-brands-svg-icons/faNodeJs";

import { faDatabase } from "@fortawesome/free-solid-svg-icons/faDatabase";
import { faCircleInfo } from "@fortawesome/free-solid-svg-icons/faCircleInfo";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export function Modal() {
  const [shouldShow, setShouldShow] = useState(false);

  function onClose() {
    setShouldShow(false);
  }
  function onOpen() {
    setShouldShow(true);
  }

  return (
    <>
      <span
        onClick={onOpen}
        className="level-item name-link"
        style={{ fontSize: "3rem" }}
      >
        <FontAwesomeIcon icon={faCircleInfo} />
      </span>
      <div className={classnames("modal", { "is-active": shouldShow })}>
        <div className="modal-background" onClick={onClose}></div>
        <div className="modal-content">
          <div className="box">
            <div className="container has-text-left">
              <div className="columns is-multiline">
                <div className="column is-full">
                  <div className="content has-text-left">
                    <h4>What&apos;s Reid Listening To?</h4>
                    <p>
                      This site showcases the breadth of my, ( Reid&apos;s )
                      full-stack capabilities. It is a&nbsp;
                      <a
                        href="https://gitlab.com/lameimpala/whats-reid-listening-to"
                        target="_blank"
                        rel="nopenner noreferrer"
                      >
                        React-based webapp
                      </a>
                      &nbsp;that shows play counts stored in a MongoDB Atlas
                      instance. Deployments are handled via&nbsp;
                      <a
                        href="https://gitlab.com/lameimpala/whats-reid-listening-to/-/pipelines"
                        target="_blank"
                        rel="noreferrer"
                      >
                        GitLab CI
                      </a>
                      , hosted on Google Cloud by way of Cloud Run.
                    </p>
                  </div>
                </div>
                <div className="column is-full">
                  <h5 className="title is-5">Technologies</h5>
                  <div className="area-of-expertise">
                    <div
                      className="content has-text-centered"
                      style={{ width: "100%" }}
                    >
                      <div className="is-size-5 area-items">
                        <div className="area-item">
                          <FontAwesomeIcon icon={faNodeJs as any} />
                          <br />
                          Node JS
                        </div>
                        <div className="area-item">
                          <FontAwesomeIcon icon={faReact as any} />
                          <br />
                          React
                        </div>
                        <div className="area-item">
                          <FontAwesomeIcon icon={faDocker as any} />
                          <br />
                          Docker
                        </div>
                      </div>
                      <div className="is-size-5 area-items">
                        <div className="area-item">
                          <FontAwesomeIcon icon={faCss3 as any} />
                          <br />
                          Bulma
                        </div>
                        <div className="area-item">
                          <FontAwesomeIcon icon={faDatabase as any} />
                          <br />
                          MongoDB
                        </div>
                        <div className="area-item">
                          <FontAwesomeIcon icon={faGoogle as any} />
                          <br />
                          Google Cloud
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <button
          className="modal-close is-large"
          aria-label="close"
          onClick={onClose}
        ></button>
      </div>
    </>
  );
}
