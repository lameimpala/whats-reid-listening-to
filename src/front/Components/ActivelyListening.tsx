import axios from "axios";
import { useEffect, useState } from "react";
import { timeSince } from "../utils";

export function ActivelyListening() {
  const [time, setTime] = useState(0);

  async function fetchTime() {
    const { data } = await axios.get("/api/last-played-at");
    setTime(data);
  }

  useEffect(() => {
    fetchTime();
  }, []);

  return new Date().valueOf() - time < 1000 * 60 * 10 ? (
    <div
      className="level"
      style={{ paddingBottom: "1rem", justifyContent: "center" }}
    >
      <div className="level-center">
        <div className="level-item">
          <h4 className="title is-3 has-vibes-font">
            last listened {timeSince(time)} ago
          </h4>
        </div>
      </div>
    </div>
  ) : null;
}
