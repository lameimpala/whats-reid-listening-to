import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMusic } from "@fortawesome/free-solid-svg-icons/faMusic";
import { faWikipediaW } from "@fortawesome/free-brands-svg-icons/faWikipediaW";
import { timeSince } from "../utils";

export function Play({
  imageData,
  lyrics,
  imageUrl,
  song,
  artist,
  album,
  time,
  wikipedia,
  artistWikipedia,
}: any) {
  return (
    <div className="card">
      <div className="card-image">
        {wikipedia ? (
          <a href={wikipedia} rel="nopenner noreferrer" target="_blank">
            <figure className="image is-1by1 image-with-link">
              <img
                src={imageUrl || `data:image/png;base64, ${imageData}`}
                alt={`Album Art for ${album}`}
              />
              <FontAwesomeIcon className="wiki-icon" icon={faWikipediaW} />
            </figure>
          </a>
        ) : (
          <figure className="image is-1by1">
            <img
              src={imageUrl || `data:image/png;base64, ${imageData}`}
              alt={`Album Art for ${album}`}
            />
          </figure>
        )}
      </div>
      <div className="card-content">
        <div
          className="subtitle has-chakra-font"
          style={{ color: "#efefef", fontSize: "1.67rem" }}
        >
          {song}
        </div>
        <div className="title has-chakra-font" style={{ fontSize: "1.67rem" }}>
          {artistWikipedia ? (
            <a
              className="cool-link"
              rel="nopenner noreferrer"
              target="_blank"
              href={artistWikipedia}
              style={{ display: "flex", alignItems: "center" }}
            >
              {artist}
            </a>
          ) : (
            <span className="cool-link">{artist}</span>
          )}
        </div>

        <div style={{ paddingTop: "0.33rem" }}>
          <em style={{ color: "#efefef" }} className="has-chakra-font">
            {timeSince(time)}&nbsp;ago
          </em>
        </div>
      </div>
      {lyrics && (
        <a
          className="card-footer"
          href={lyrics}
          rel="nopenner noreferrer"
          target="_blank"
        >
          <p className="card-footer-item">
            <span className="has-minimal-font cool-link">
              view on genius&nbsp;
              <FontAwesomeIcon icon={faMusic} />
            </span>
          </p>
        </a>
      )}
    </div>
  );
}
/*
s&nbsp;&nbsp;&nbsp;
              <FontAwesomeIcon icon={faExternalLink} />

*/
