import dateFormat from "dateformat";

export function Artist({ imageUrl, artist, album, time, plays }: any) {
  return (
    <div className="card" style={{ margin: "1.5em" }}>
      <div className="card-image">
        <figure className="image is-1by1">
          <img
            src={imageUrl}
            title={`Album Art for ${album}`}
            alt={`Album Art for ${album}`}
          />
        </figure>
      </div>
      <div className="card-content">
        <div className="content">
          <h5 className="title is-5">{artist}</h5>
          <h6 className="subtitle is-6">Overall Play Count: {plays}</h6>
          <small>
            <strong>Last played at </strong>
          </small>
          <br />
          <small title="Last Played Time">
            <em>{dateFormat(time, "dddd, mmmm dS, yyyy")}</em>
            <br />
            <em>{dateFormat(time, '"on" h:MM TT')}</em>
          </small>
        </div>
      </div>
    </div>
  );
}
