import { Plays } from "./Plays";

export function Landing() {
  return (
    <div className="container" style={{ padding: "2rem" }}>
      <Plays />
    </div>
  );
}

/*
      <div className="level">
        <div className="level-left">
          <h2 className="title is-2 has-vibes-font">
            {shouldShowSongs ? "Recently Played Songs" : "Most Played Arists"}
          </h2>
        </div>
        <div className="level-right">
          <div className="level-item">
            <button
              className="button has-vibes-font"
              onClick={() => {
                setShouldShowSongs(!shouldShowSongs);
              }}
            >
              Show&nbsp;{shouldShowSongs ? "Top Artists" : "Recent Songs"}
            </button>
          </div>
        </div>
      </div>
*/
