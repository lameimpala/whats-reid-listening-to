import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Modal } from "./Modal";
import { faUserAstronaut } from "@fortawesome/free-solid-svg-icons/faUserAstronaut";

export function Nav() {
  return (
    <nav
      className="level"
      role="navigation"
      aria-label="main navigation"
      style={{ paddingTop: "2rem", paddingLeft: "3rem", paddingRight: "3rem" }}
    >
      <div className="level-left">
        <Modal />
      </div>
      <div className="level-center">
        <div className="level-item">
          <a
            href="https://whatsreidlistening.to"
            rel="self"
            className="name-link has-minimal-font"
            style={{ fontSize: "1.75rem" }}
          >
            whatsreidlistening.to
          </a>
        </div>
      </div>

      <div className="level-right">
        <a
          href="https://reidwixom.com/"
          className="level-item name-link"
          rel="nopenner noreferrer"
          target="_blank"
        >
          <FontAwesomeIcon icon={faUserAstronaut} />
        </a>
      </div>
    </nav>
  );
}

/*
      <div className="level-center">
        <div className="level-item">
          <ActivelyListening />
        </div>
      </div>
      */
