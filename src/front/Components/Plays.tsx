import { faSpinner } from "@fortawesome/free-solid-svg-icons/faSpinner";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import { useEffect, useState } from "react";
import { Play } from "./Play";

export function Plays() {
  const [pageIndex, setPageIndex] = useState(0);
  const [plays, setPlays] = useState([]);

  async function fetchPlays(i: number) {
    const { data } = await axios.get(`/api/plays/${i}`);
    setPlays([...plays, ...data]);
    setPageIndex(i + 1);
  }

  useEffect(() => {
    if (pageIndex <= 25) {
      fetchPlays(pageIndex);
    }
  }, [pageIndex]);

  return (
    <div className="columns is-mobile is-multiline is-fullwidth is-fullheight">
      {plays.length ? (
        plays.map((play) => (
          <div
            key={play._id}
            className="column is-full-mobile is-half-tablet is-one-third-desktop is-one-quarter-widescreen"
          >
            <Play {...play} />
          </div>
        ))
      ) : (
        <div className="column is-full has-text-centered">
          <FontAwesomeIcon icon={faSpinner} spin size="4x" />
        </div>
      )}
    </div>
  );
}
