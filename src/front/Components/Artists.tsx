import axios from "axios";
import { useEffect, useState } from "react";
import { Artist } from "./Artist";

export function Artists() {
  const [artists, setArtists] = useState([]);

  async function fetchArtists() {
    const { data } = await axios.get("/api/artists");
    setArtists(data);
  }

  useEffect(() => {
    fetchArtists();
  }, []);

  return (
    <div className="columns is-mobile is-multiline is-fullwidth">
      {artists.map((artist) => (
        <div
          key={artist._id}
          className="column is-half-mobile is-one-third-tablet is-one-quarter-desktop is-one-fifth-widescreen"
        >
          <Artist {...artist} />
        </div>
      ))}
    </div>
  );
}
