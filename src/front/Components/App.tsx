import { Landing } from "./Landing";
import { Nav } from "./Nav";

export function App() {
  return (
    <div className="container">
      <Nav />
      <Landing />
    </div>
  );
}
